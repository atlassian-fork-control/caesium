package com.atlassian.scheduler.caesium.impl;

import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.caesium.cron.CaesiumCronFactory;
import com.atlassian.scheduler.caesium.spi.CaesiumSchedulerConfiguration;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.core.spi.SchedulerServiceConfiguration;
import com.atlassian.scheduler.core.tests.AbstractCronExpressionTest;
import com.atlassian.scheduler.core.tests.CronFactory.CronExpressionAdapter;
import com.atlassian.scheduler.cron.CronSyntaxException;
import org.hamcrest.Matcher;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.annotation.Nullable;
import java.util.Date;

import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.runTime;
import static java.util.concurrent.TimeUnit.MINUTES;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Covers {@link RunTimeCalculator}, as opposed to {@link SchedulerService#calculateNextRunTime(Schedule)},
 * which has its own {@link CaesiumCalculateNextRunTimeTest}.
 *
 * @since v0.0.1
 */
@SuppressWarnings("ConstantConditions")
public class RunTimeCalculatorTest extends AbstractCronExpressionTest {
    private static final JobId JOB_ID = JobId.of("JobId");
    private static final JobRunnerKey JOB_RUNNER_KEY = JobRunnerKey.of("JobRunnerKey");

    @Rule
    public MockitoRule init = MockitoJUnit.rule();

    @Mock
    CaesiumSchedulerConfiguration config;

    long now = System.currentTimeMillis();
    Fixture calculator;


    @Before
    public void setUp() {
        calculator = new Fixture(config);
    }

    public RunTimeCalculatorTest() {
        super(new CaesiumCronFactory());
    }

    @Test
    public void firstRunTimeForRunOnceWithoutFirstRunTimeIsNow() throws SchedulerServiceException {
        final Date next = calculator.firstRunTime(JOB_ID, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY));

        assertDate(now, next);
    }

    @Test
    public void nextRunTimeForRunOnceWithoutFirstRunTimeIsNow() throws SchedulerServiceException {
        final Date next = calculator.nextRunTime(Schedule.runOnce(null), null);

        assertDate(now, next);
    }

    @Test
    public void firstRunTimeForRunOnceWithFirstRunTimeInFutureIsThatTime() throws SchedulerServiceException {
        final Date firstRun = new Date(now + 120000L);
        final Date next = calculator.firstRunTime(JOB_ID, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY)
                .withSchedule(Schedule.runOnce(firstRun)));

        assertDate(firstRun, next);
    }

    @Test
    public void firstRunTimeForRunOnceWithFirstRunTimeInPastIsNow() throws SchedulerServiceException {
        final Date firstRun = new Date(now - 120000L);
        final Date next = calculator.firstRunTime(JOB_ID, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY)
                .withSchedule(Schedule.runOnce(firstRun)));

        assertDate(now, next);
    }

    @Test
    public void nextRunTimeForRunOnceWithFirstRunTimeInFutureIsThatTime() throws SchedulerServiceException {
        final Date firstRun = new Date(now + 120000L);
        final Date next = calculator.nextRunTime(Schedule.runOnce(firstRun), null);

        assertDate(firstRun, next);
    }

    @Test
    public void nextRunTimeForRunOnceWithFirstRunTimeInPastIsNow() throws SchedulerServiceException {
        final Date firstRun = new Date(now - 120000L);
        final Date next = calculator.nextRunTime(Schedule.runOnce(firstRun), null);

        assertDate(now, next);
    }

    @Test
    public void nextRunTimeForRunOnceThatHasPreviousRunIsNullRegardlessOfFirstRunDate() throws SchedulerServiceException {
        final Date earlier = new Date(now - 120000L);
        final Date later = new Date(now + 120000L);

        assertThat(calculator.nextRunTime(Schedule.runOnce(null), earlier), nullValue());
        assertThat(calculator.nextRunTime(Schedule.runOnce(earlier), earlier), nullValue());
        assertThat(calculator.nextRunTime(Schedule.runOnce(later), earlier), nullValue());
        assertThat(calculator.nextRunTime(Schedule.runOnce(earlier), later), nullValue());
        assertThat(calculator.nextRunTime(Schedule.runOnce(later), later), nullValue());
    }

    @Test
    public void firstRunTimeForIntervalWithoutFirstRunTimeIsNow() throws SchedulerServiceException {
        final Date next = calculator.firstRunTime(JOB_ID, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY)
                .withSchedule(Schedule.forInterval(60000L, null)));

        assertDate(now, next);
    }

    @Test
    public void firstRunTimeForIntervalWithFirstRunTimeInFutureIsThatTime() throws SchedulerServiceException {
        final Date firstRun = new Date(now + 120000L);
        final Date next = calculator.firstRunTime(JOB_ID, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY)
                .withSchedule(Schedule.forInterval(60000L, firstRun)));

        assertDate(firstRun, next);
    }

    @Test
    public void firstRunTimeForIntervalWithFirstRunTimeInPastIsNow() throws SchedulerServiceException {
        final Date firstRun = new Date(now - 120000L);
        final Date next = calculator.firstRunTime(JOB_ID, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY)
                .withSchedule(Schedule.forInterval(60000L, firstRun)));

        assertDate(now, next);
    }

    @Test
    public void nextRunTimeForIntervalWithPreviousRunTimeIsBasedOnThat() throws SchedulerServiceException {
        final Date firstRun = new Date(now - 240000L);
        final Date thisRun = new Date(now - 60000L);
        final Date next = calculator.nextRunTime(Schedule.forInterval(120000L, firstRun), thisRun);

        assertDate(now + 60000L, next);
    }

    // The cron expression tests have better coverage for all the little crazy things associated with
    // cron schedules.  Just testing enough here to make sure time zones are getting used properly.

    @Test
    public void nextRunTimeForCronExpressionUsingSydneyDstGap() throws SchedulerServiceException {
        final DateTimeZone zone = DateTimeZone.forID("Australia/Sydney");
        final DateTime startingAt = new DateTime(2014, 10, 4, 23, 30, 0, zone);
        final long base = startingAt.getMillis();

        // Get time zone from config
        when(config.getDefaultTimeZone()).thenReturn(zone.toTimeZone());

        final Schedule schedule1 = Schedule.forCronExpression("0 0 * * * ?");
        assertRunTimes("Australia/Sydney; 2014/10/05; 02 => 03 (On the hour)", schedule1, startingAt,
                runTime(0, 0, 0, 5, 10, 2014, zone, base + MINUTES.toMillis(30)),
                runTime(0, 0, 1, 5, 10, 2014, zone, base + MINUTES.toMillis(90)),
                runTime(0, 0, 3, 5, 10, 2014, zone, base + MINUTES.toMillis(150)),
                runTime(0, 0, 4, 5, 10, 2014, zone, base + MINUTES.toMillis(210)));

        final Schedule schedule2 = Schedule.forCronExpression("0 30 * * * ?");
        assertRunTimes("Australia/Sydney; 2014/10/05; 02 => 03 (Within the hour)", schedule2, startingAt,
                runTime(0, 30, 0, 5, 10, 2014, zone, base + MINUTES.toMillis(60)),
                runTime(0, 30, 1, 5, 10, 2014, zone, base + MINUTES.toMillis(120)),
                runTime(0, 30, 3, 5, 10, 2014, zone, base + MINUTES.toMillis(180)),
                runTime(0, 30, 4, 5, 10, 2014, zone, base + MINUTES.toMillis(240)));

        verify(config, atLeastOnce()).getDefaultTimeZone();
        verifyNoMoreInteractions(config);
    }

    @Test
    public void nextRunTimeForCronExpressionUsingSydneyDstOverlap() throws SchedulerServiceException {
        final DateTimeZone zone = DateTimeZone.forID("Australia/Sydney");
        final DateTime startingAt = new DateTime(2014, 4, 5, 23, 30, 0, zone);
        final long base = startingAt.getMillis();

        // Get time zone from cron expression
        final Schedule schedule = Schedule.forCronExpression("0 30 * * * ?", zone.toTimeZone());

        // The first 01:30 runs, the second one is skipped, and we run again at 02:30.  The result is
        // that the runs at 01:30 and 02:30 are 2 hours apart instead of 1 hour.
        assertRunTimes("Australia/Sydney; 2014/04/06", schedule, startingAt,
                runTime(0, 30, 0, 6, 4, 2014, zone, base + MINUTES.toMillis(60)),
                runTime(0, 30, 1, 6, 4, 2014, zone, base + MINUTES.toMillis(120)),
                runTime(0, 30, 2, 6, 4, 2014, zone, base + MINUTES.toMillis(240)),
                runTime(0, 30, 3, 6, 4, 2014, zone, base + MINUTES.toMillis(300)),
                runTime(0, 30, 4, 6, 4, 2014, zone, base + MINUTES.toMillis(360)));

        verifyZeroInteractions(config);
    }


    /**
     * Lord Howe Island, Australia just had to be different.
     * <p>
     * Its daylight savings transition is 30 minutes instead of a full hour.  Welcome to crazy town!
     * </p>
     */
    @Test
    public void nextRunTimeForCronExpressionUsingLordHoweDstGap() throws SchedulerServiceException {
        final DateTimeZone zone = DateTimeZone.forID("Australia/Lord_Howe");
        final DateTime startingAt = new DateTime(2014, 10, 4, 23, 30, 0, zone);
        final long base = startingAt.getMillis();

        // Get time zone from config
        when(config.getDefaultTimeZone()).thenReturn(zone.toTimeZone());
        final Schedule schedule = Schedule.forCronExpression("0 0 * * * ?");

        assertRunTimes("Australia/Lord_Howe; 2014/10/05", schedule, startingAt,
                runTime(0, 0, 0, 5, 10, 2014, zone, base + MINUTES.toMillis(30)),
                runTime(0, 0, 1, 5, 10, 2014, zone, base + MINUTES.toMillis(90)),
                runTime(0, 0, 3, 5, 10, 2014, zone, base + MINUTES.toMillis(180)),
                runTime(0, 0, 4, 5, 10, 2014, zone, base + MINUTES.toMillis(240)));
    }

    @Test
    public void nextRunTimeForCronExpressionUsingLordHoweDstOverlap() throws SchedulerServiceException {

        final DateTimeZone zone = DateTimeZone.forID("Australia/Lord_Howe");
        final DateTime startingAt = new DateTime(2014, 4, 5, 23, 30, 0, zone);
        final long base = startingAt.getMillis();

        // Get time zone from cron expression
        final Schedule schedule = Schedule.forCronExpression("0 30 * * * ?", zone.toTimeZone());

        // 00:30 and 01:30 are *90 minutes* apart.
        assertRunTimes("Australia/Lord_Howe; 2014/04/06", schedule, startingAt,
                runTime(0, 30, 0, 6, 4, 2014, zone, base + MINUTES.toMillis(60)),
                runTime(0, 30, 1, 6, 4, 2014, zone, base + MINUTES.toMillis(150)),
                runTime(0, 30, 2, 6, 4, 2014, zone, base + MINUTES.toMillis(210)),
                runTime(0, 30, 3, 6, 4, 2014, zone, base + MINUTES.toMillis(270)),
                runTime(0, 30, 4, 6, 4, 2014, zone, base + MINUTES.toMillis(330)));
    }

    @Test
    public void nextRuntimeForCronExpressionInThePast() throws SchedulerServiceException {
        final DateTimeZone zone = DateTimeZone.forID("Australia/Lord_Howe");
        final Schedule schedule = Schedule.forCronExpression("0 * * * * ?", zone.toTimeZone());
        final DateTime startingAt = new DateTime(2014, 4, 5, 23, 30, 0, zone);
        final RunTimeCalculator calculator = new RunTimeCalculator(config);
        final Date nextRunResult = calculator.nextRunTime(schedule, startingAt.toDate());
        assertDate(startingAt.plusMinutes(1).toDate(), nextRunResult);
    }

    protected void assertRunTimes(final String message, final Schedule cronSchedule, final DateTime startingAfter,
                                  final Matcher... matchers) {
        final CronExpressionAdapter adapter = new CronExpressionAdapter() {
            @Override
            public boolean isSatisfiedBy(Date date) {
                throw new AssertionError("Shouldn't be called");
            }

            @Nullable
            @Override
            public Date nextRunTime(Date dateTime) {
                final long savedNow = now;
                try {
                    now = dateTime.getTime();
                    return calculator.nextRunTime(cronSchedule, dateTime);
                } catch (CronSyntaxException cse) {
                    throw new AssertionError(cse);
                } finally {
                    now = savedNow;
                }
            }
        };

        assertRunTimes(message, adapter, startingAfter, matchers);
    }

    private static void assertDate(final long expectedMillis, final Date actualDate) {
        assertDate(new Date(expectedMillis), actualDate);
    }

    private static void assertDate(final Date expectedDate, final Date actualDate) {
        assertThat(actualDate, equalTo(expectedDate));
    }


    class Fixture extends RunTimeCalculator {
        public Fixture(SchedulerServiceConfiguration config) {
            super(config);
        }

        /**
         * Returns a new date that respects our hard-coded clock.
         */
        @Override
        Date now() {
            return new Date(now);
        }
    }
}
