package com.atlassian.scheduler.caesium.cron;

import com.atlassian.scheduler.core.tests.CronExpressionValidatorTest;
import org.junit.Before;
import org.junit.Test;

/**
 * @since v0.0.1
 */
public class CaesiumCronExpressionValidatorTest extends CronExpressionValidatorTest {
    @Before
    public void setUp() {
        validator = new CaesiumCronExpressionValidator();
    }

    @Test
    public void testLeadingWhitespace() {
        assertValid(" \t 0 0 0 ? 1 MON-TUE");
    }
}
