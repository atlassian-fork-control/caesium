package com.atlassian.scheduler.caesium.impl;

import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.caesium.spi.CaesiumSchedulerConfiguration;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.core.impl.MemoryRunDetailsDao;
import com.atlassian.scheduler.core.tests.CalculateNextRunTimeTest;
import org.junit.Before;
import org.junit.Rule;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

/**
 * Tests {@link SchedulerService#calculateNextRunTime(Schedule)}, as opposed to
 * {@link RunTimeCalculator}, which has its own {@link RunTimeCalculatorTest} to cover it.
 *
 * @since v0.0.3
 */
public class CaesiumCalculateNextRunTimeTest extends CalculateNextRunTimeTest {
    @Rule
    public MockitoRule init = MockitoJUnit.rule();

    @Captor
    private ArgumentCaptor<Schedule> scheduleCaptor;

    @Mock
    private CaesiumSchedulerConfiguration config;

    private SchedulerService schedulerService;

    @Before
    public void setUp() throws SchedulerServiceException {
        this.schedulerService = new CaesiumSchedulerService(config, new MemoryRunDetailsDao(),
                new MemoryClusteredJobDao());
    }

    @Override
    protected SchedulerService getSchedulerService() {
        return schedulerService;
    }
}
