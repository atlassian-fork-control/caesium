package com.atlassian.scheduler.caesium.cron;

import com.atlassian.scheduler.core.tests.CronExpressionHoursTest;

/**
 * @since v0.0.1
 */
public class CaesiumCronExpressionHoursTest extends CronExpressionHoursTest {
    public CaesiumCronExpressionHoursTest() {
        super(new CaesiumCronFactory());
    }
}
