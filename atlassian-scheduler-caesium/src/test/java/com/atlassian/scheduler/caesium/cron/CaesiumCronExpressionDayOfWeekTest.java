package com.atlassian.scheduler.caesium.cron;

import com.atlassian.scheduler.core.tests.CronExpressionDayOfWeekTest;

/**
 * @since v0.0.1
 */
public class CaesiumCronExpressionDayOfWeekTest extends CronExpressionDayOfWeekTest {
    public CaesiumCronExpressionDayOfWeekTest() {
        super(new CaesiumCronFactory());
    }
}
