package com.atlassian.scheduler.caesium.impl;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.caesium.spi.CaesiumSchedulerConfiguration;
import com.atlassian.scheduler.caesium.spi.ClusteredJob;
import com.atlassian.scheduler.caesium.spi.ClusteredJobDao;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.core.impl.MemoryRunDetailsDao;
import com.atlassian.scheduler.core.spi.RunDetailsDao;
import com.atlassian.scheduler.core.util.ParameterMapSerializer;
import com.atlassian.scheduler.status.JobDetails;
import com.atlassian.scheduler.status.RunDetails;
import com.atlassian.scheduler.status.RunOutcome;
import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.annotation.Nullable;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import static com.atlassian.scheduler.caesium.impl.CaesiumSchedulerService.RECOVERY_JOB_ID;
import static com.atlassian.scheduler.caesium.impl.CaesiumSchedulerService.RECOVERY_JOB_RUNNER_KEY;
import static com.atlassian.scheduler.caesium.impl.CaesiumSchedulerService.REFRESH_JOB_ID;
import static com.atlassian.scheduler.caesium.impl.CaesiumSchedulerService.REFRESH_JOB_RUNNER_KEY;
import static com.atlassian.scheduler.config.RunMode.RUN_LOCALLY;
import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static com.atlassian.scheduler.config.Schedule.forCronExpression;
import static com.atlassian.scheduler.config.Schedule.forInterval;
import static com.atlassian.scheduler.config.Schedule.runOnce;
import static com.atlassian.utt.mock.MoreAnswers.reject;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v0.0.1
 */
public class CaesiumSchedulerServiceTest {
    private static final JobId JOB_ID_1 = JobId.of("JobId1");
    private static final JobId JOB_ID_2 = JobId.of("JobId2");
    private static final JobId JOB_ID_3 = JobId.of("JobId3");
    private static final JobId JOB_ID_4 = JobId.of("JobId4");
    private static final JobRunnerKey JOB_RUNNER_KEY_1 = JobRunnerKey.of("JobRunnerKey1");
    private static final JobRunnerKey JOB_RUNNER_KEY_2 = JobRunnerKey.of("JobRunnerKey2");
    private static final JobRunnerKey JOB_RUNNER_KEY_3 = JobRunnerKey.of("JobRunnerKey3");
    private static final Map<String, Serializable> PARAMETERS = ImmutableMap.of(
            "hello", "world",
            "another", 42L);
    private static final byte[] RAW_PARAMETERS;
    private static final String EVERY_SECOND = "* * * * * ?";
    private static final TimeZone SYDNEY = TimeZone.getTimeZone("Australia/Sydney");

    static {
        try {
            RAW_PARAMETERS = new ParameterMapSerializer().serializeParameters(PARAMETERS);
        } catch (SchedulerServiceException sse) {
            throw new AssertionError(sse);
        }
    }

    @Rule
    public MockitoRule init = MockitoJUnit.rule().silent();

    @Mock
    private CaesiumSchedulerConfiguration config;
    @Mock
    private SchedulerQueue queue;
    @Mock
    private RunTimeCalculator calculator;
    @Mock
    private ClusteredJobDao clusteredJobDao;

    private RunDetailsDao runDetailsDao = new MemoryRunDetailsDao();

    private CaesiumSchedulerService schedulerService;
    private long now = 10000L;

    @Before
    public void setUp() {
        createSchedulerService();
    }

    private void createSchedulerService() {
        this.schedulerService = new CaesiumSchedulerService(config, runDetailsDao, clusteredJobDao,
                queue, calculator) {
            @Override
            long now() {
                return now;
            }
        };
    }

    @After
    public void tearDown() {
        schedulerService.shutdown();
    }

    @Test
    public void testScheduleLocalJob() throws SchedulerServiceException {
        final JobConfig jobConfig = JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1)
                .withRunMode(RUN_LOCALLY)
                .withParameters(PARAMETERS);
        when(calculator.firstRunTime(eq(JOB_ID_1), same(jobConfig))).thenReturn(new Date(now));

        schedulerService.scheduleJob(JOB_ID_1, jobConfig);

        verify(queue).add(new QueuedJob(JOB_ID_1, now));
        verify(clusteredJobDao).delete(JOB_ID_1);
        assertThat("should get the right job details", schedulerService.getJobDetails(JOB_ID_1), jobDetails(
                JOB_ID_1, JOB_RUNNER_KEY_1, RUN_LOCALLY,
                runOnce(null), new Date(now), RAW_PARAMETERS));

        verifyNoMoreInteractions(clusteredJobDao);
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void testScheduleQuantizedCronExpression() throws SchedulerServiceException {
        final JobConfig jobConfig = JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1)
                .withSchedule(forCronExpression(EVERY_SECOND, SYDNEY))
                .withRunMode(RUN_LOCALLY)
                .withParameters(PARAMETERS);
        when(calculator.firstRunTime(eq(JOB_ID_1), same(jobConfig))).thenReturn(new Date(now));

        schedulerService.scheduleJob(JOB_ID_1, jobConfig);

        final JobDetails jobDetails = schedulerService.getJobDetails(JOB_ID_1);
        final String quantized = jobDetails.getSchedule().getCronScheduleInfo().getCronExpression();
        final java.util.regex.Matcher matcher = Pattern.compile("(\\d+) \\* \\* \\* \\* \\?").matcher(quantized);
        assertThat(quantized, matcher.matches(), is(true));
        final int seconds = Integer.parseInt(matcher.group(1));
        assertThat(seconds, allOf(greaterThanOrEqualTo(0), lessThan(60)));

        assertThat("should get the right job details", schedulerService.getJobDetails(JOB_ID_1), jobDetails(
                JOB_ID_1, JOB_RUNNER_KEY_1, RUN_LOCALLY,
                forCronExpression(quantized, SYDNEY), new Date(now), RAW_PARAMETERS));
    }

    @Test
    public void testScheduleFineGrainedCronExpression() throws SchedulerServiceException {
        final JobConfig jobConfig = JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1)
                .withSchedule(forCronExpression(EVERY_SECOND, SYDNEY))
                .withRunMode(RUN_LOCALLY)
                .withParameters(PARAMETERS);
        when(config.useFineGrainedSchedules()).thenReturn(true);
        when(calculator.firstRunTime(eq(JOB_ID_1), same(jobConfig))).thenReturn(new Date(now));

        schedulerService.scheduleJob(JOB_ID_1, jobConfig);

        assertThat("should get the right job details", schedulerService.getJobDetails(JOB_ID_1), jobDetails(
                JOB_ID_1, JOB_RUNNER_KEY_1, RUN_LOCALLY,
                forCronExpression(EVERY_SECOND, SYDNEY), new Date(now), RAW_PARAMETERS));
    }

    @Test
    public void testScheduleQuantizedInterval() throws SchedulerServiceException {
        final JobConfig jobConfig = JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1)
                .withSchedule(forInterval(30000L, null))
                .withRunMode(RUN_LOCALLY)
                .withParameters(PARAMETERS);
        when(calculator.firstRunTime(eq(JOB_ID_1), same(jobConfig))).thenReturn(new Date(now));

        schedulerService.scheduleJob(JOB_ID_1, jobConfig);

        assertThat("should get the right job details", schedulerService.getJobDetails(JOB_ID_1), jobDetails(
                JOB_ID_1, JOB_RUNNER_KEY_1, RUN_LOCALLY,
                forInterval(60000L, null), new Date(now), RAW_PARAMETERS));
    }

    @Test
    public void testScheduleFineGrainedInterval() throws SchedulerServiceException {
        final JobConfig jobConfig = JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1)
                .withSchedule(forInterval(30000L, null))
                .withRunMode(RUN_LOCALLY)
                .withParameters(PARAMETERS);
        when(config.useFineGrainedSchedules()).thenReturn(true);
        when(calculator.firstRunTime(eq(JOB_ID_1), same(jobConfig))).thenReturn(new Date(now));

        schedulerService.scheduleJob(JOB_ID_1, jobConfig);

        assertThat("should get the right job details", schedulerService.getJobDetails(JOB_ID_1), jobDetails(
                JOB_ID_1, JOB_RUNNER_KEY_1, RUN_LOCALLY,
                forInterval(30000L, null), new Date(now), RAW_PARAMETERS));
    }

    @Test
    public void testScheduleClusteredJobSuccess() throws SchedulerServiceException {
        clusteredJobDao = new MemoryClusteredJobDao();
        createSchedulerService();

        // Start out with a local job already scheduled
        final JobConfig localJobConfig = JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1).withRunMode(RUN_LOCALLY);
        schedulerService.scheduleJob(JOB_ID_1, localJobConfig);
        when(calculator.firstRunTime(eq(JOB_ID_1), same(localJobConfig))).thenReturn(new Date(now + 1234L));
        reset(calculator, queue);

        final JobConfig jobConfig = JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1).withParameters(PARAMETERS);
        when(calculator.firstRunTime(eq(JOB_ID_1), same(jobConfig))).thenReturn(new Date(now));
        schedulerService.registerJobRunner(JOB_RUNNER_KEY_1, new TestJobRunner());

        schedulerService.scheduleJob(JOB_ID_1, jobConfig);

        verify(queue).add(new QueuedJob(JOB_ID_1, now));
        assertThat("should get the right job details", schedulerService.getJobDetails(JOB_ID_1), jobDetails(
                JOB_ID_1, JOB_RUNNER_KEY_1, RUN_ONCE_PER_CLUSTER,
                runOnce(null), new Date(now), RAW_PARAMETERS));
    }

    @Test
    public void testScheduleClusteredJobSuccessOnRetry() throws SchedulerServiceException {
        final JobConfig jobConfig = JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1).withParameters(PARAMETERS);
        when(calculator.firstRunTime(eq(JOB_ID_1), same(jobConfig))).thenReturn(new Date(now));
        //noinspection unchecked
        when(clusteredJobDao.create(any(ClusteredJob.class)))
                .thenReturn(false)
                .thenReturn(true)
                .thenThrow(AssertionError.class);
        schedulerService.registerJobRunner(JOB_RUNNER_KEY_1, new TestJobRunner());

        schedulerService.scheduleJob(JOB_ID_1, jobConfig);

        verify(queue).add(new QueuedJob(JOB_ID_1, now));
    }

    @Test
    public void testScheduleClusteredJobFailure() throws SchedulerServiceException {
        final JobConfig jobConfig = JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1).withParameters(PARAMETERS);
        when(calculator.firstRunTime(eq(JOB_ID_1), same(jobConfig))).thenReturn(new Date(now));
        schedulerService.registerJobRunner(JOB_RUNNER_KEY_1, new TestJobRunner());

        try {
            schedulerService.scheduleJob(JOB_ID_1, jobConfig);
        } catch (SchedulerServiceException sse) {
            assertThat(sse.getMessage(), containsString("Unable to either create or replace"));
            verifyZeroInteractions(queue);
        }
    }

    @Test
    public void testUnscheduleJobSucceeds() throws SchedulerServiceException {
        // Start out with a local job already scheduled
        final JobConfig localJobConfig = JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1).withRunMode(RUN_LOCALLY);
        schedulerService.scheduleJob(JOB_ID_1, localJobConfig);
        when(calculator.firstRunTime(eq(JOB_ID_1), same(localJobConfig))).thenReturn(new Date(now));
        reset(calculator, clusteredJobDao, queue);

        schedulerService.unscheduleJob(JOB_ID_1);

        verify(clusteredJobDao).delete(JOB_ID_1);
        verify(queue).remove(JOB_ID_1);
        assertThat("job details should be gone", schedulerService.getJobDetails(JOB_ID_1), nullValue());
    }

    @Test
    public void testUnscheduleJobWhenItDoesNotExist() throws SchedulerServiceException {
        // Start out with a local job already scheduled
        final JobConfig localJobConfig = JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1).withRunMode(RUN_LOCALLY);
        when(calculator.firstRunTime(eq(JOB_ID_1), same(localJobConfig))).thenReturn(new Date(now));
        schedulerService.scheduleJob(JOB_ID_1, localJobConfig);
        reset(calculator, clusteredJobDao, queue);

        schedulerService.unscheduleJob(JOB_ID_2);

        verify(clusteredJobDao).delete(JOB_ID_2);
        verify(queue).remove(JOB_ID_2);
        assertThat("Other job details should be unaffected", schedulerService.getJobDetails(JOB_ID_1), jobDetails(
                JOB_ID_1, JOB_RUNNER_KEY_1, RUN_LOCALLY,
                runOnce(null), new Date(now), null));
    }

    @Test
    public void testGetJobRunnerKeysForAllScheduledJobs() throws SchedulerServiceException {
        when(calculator.firstRunTime(any(JobId.class), any(JobConfig.class))).thenReturn(new Date(now));
        schedulerService.scheduleJob(JOB_ID_1, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1).withRunMode(RUN_LOCALLY));
        schedulerService.scheduleJob(JOB_ID_2, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_2).withRunMode(RUN_LOCALLY));

        when(clusteredJobDao.findAllJobRunnerKeys()).thenReturn(ImmutableSet.of(JOB_RUNNER_KEY_2, JOB_RUNNER_KEY_3));

        assertThat(schedulerService.getJobRunnerKeysForAllScheduledJobs(), contains(
                JOB_RUNNER_KEY_1, JOB_RUNNER_KEY_2, JOB_RUNNER_KEY_3));
    }

    @Test
    public void testGetJobsByJobRunnerKey() throws SchedulerServiceException {
        when(calculator.firstRunTime(any(JobId.class), any(JobConfig.class))).thenReturn(new Date(now));
        schedulerService.scheduleJob(JOB_ID_1, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1).withRunMode(RUN_LOCALLY));
        schedulerService.scheduleJob(JOB_ID_2, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_2).withRunMode(RUN_LOCALLY));
        schedulerService.scheduleJob(JOB_ID_4, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_2).withRunMode(RUN_LOCALLY));
        schedulerService.registerJobRunner(JOB_RUNNER_KEY_2, new TestJobRunner());

        final ClusteredJob cj2 = ImmutableClusteredJob.builder()
                .jobId(JOB_ID_2)
                .jobRunnerKey(JOB_RUNNER_KEY_2)
                .schedule(Schedule.forCronExpression("0 * * * * ?"))
                .nextRunTime(null)
                .build();
        final ClusteredJob cj3 = ImmutableClusteredJob.builder()
                .jobId(JOB_ID_3)
                .jobRunnerKey(JOB_RUNNER_KEY_2)
                .nextRunTime(new Date(now + 4200L))
                .parameters(RAW_PARAMETERS)
                .build();
        when(clusteredJobDao.findByJobRunnerKey(JOB_RUNNER_KEY_2)).thenReturn(ImmutableSet.of(cj2, cj3));

        final List<JobDetails> jobs = schedulerService.getJobsByJobRunnerKey(JOB_RUNNER_KEY_2);

        // Used to fix the otherwise broken generics
        //noinspection RedundantArrayCreation
        assertThat(jobs, contains(new Matcher[]{
                jobDetails(JOB_ID_2, JOB_RUNNER_KEY_2, RUN_ONCE_PER_CLUSTER,
                        Schedule.forCronExpression("0 * * * * ?"), null, null),
                jobDetails(JOB_ID_3, JOB_RUNNER_KEY_2, RUN_ONCE_PER_CLUSTER,
                        runOnce(null), new Date(now + 4200L), RAW_PARAMETERS),
                jobDetails(JOB_ID_4, JOB_RUNNER_KEY_2, RUN_LOCALLY,
                        runOnce(null), new Date(now), null)
        }));
    }

    @Test
    public void testRefreshJobScheduledIfConfigured() throws SchedulerServiceException {
        final int intervalMinutes = 42;
        final long intervalMillis = TimeUnit.MINUTES.toMillis(intervalMinutes);
        final long refreshAt = now + intervalMillis;
        final Date runTime = new Date(refreshAt);

        when(config.refreshClusteredJobsIntervalInMinutes()).thenReturn(intervalMinutes);
        when(calculator.firstRunTime(eq(REFRESH_JOB_ID), any(JobConfig.class))).thenReturn(runTime);

        schedulerService.scheduleRefreshJob();

        verify(queue).add(new QueuedJob(REFRESH_JOB_ID, refreshAt));
        assertThat(schedulerService.getJobRunner(REFRESH_JOB_RUNNER_KEY),
                instanceOf(CaesiumSchedulerService.RefreshJob.class));

        final JobDetails jobDetails = schedulerService.getJobDetails(REFRESH_JOB_ID);
        assertNotNull(jobDetails);
        assertThat(jobDetails.getJobRunnerKey(), is(REFRESH_JOB_RUNNER_KEY));
        assertThat(jobDetails.getSchedule(), is(Schedule.forInterval(intervalMillis, runTime)));
    }

    @Test
    public void testRefreshJobNotScheduledIfDisabled() throws SchedulerServiceException {
        final int intervalMinutes = 0;

        when(config.refreshClusteredJobsIntervalInMinutes()).thenReturn(intervalMinutes);
        reject(calculator).firstRunTime(eq(REFRESH_JOB_ID), any(JobConfig.class));

        schedulerService.scheduleRefreshJob();

        verify(queue).remove(REFRESH_JOB_ID);
        assertThat(schedulerService.getJobRunner(REFRESH_JOB_RUNNER_KEY), nullValue());
    }

    @Test
    public void testRefreshClusteredJobWhenItIsLocal() throws SchedulerServiceException {
        when(calculator.firstRunTime(any(JobId.class), any(JobConfig.class))).thenReturn(new Date(now));
        schedulerService.scheduleJob(JOB_ID_1, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1).withRunMode(RUN_LOCALLY));
        reset(clusteredJobDao, queue);

        schedulerService.refreshClusteredJob(JOB_ID_1);

        verify(clusteredJobDao).getNextRunTime(JOB_ID_1);
        verifyNoMoreInteractions(clusteredJobDao, queue);
    }

    @Test
    public void testRefreshClusteredJobWithNoNextRunTime() {
        schedulerService.refreshClusteredJob(JOB_ID_1);

        verify(clusteredJobDao).getNextRunTime(JOB_ID_1);
        verify(queue).remove(JOB_ID_1);
        verifyNoMoreInteractions(clusteredJobDao, queue);
    }

    @Test
    public void testRefreshClusteredJobReplacesLocal() throws SchedulerServiceException {
        when(calculator.firstRunTime(any(JobId.class), any(JobConfig.class))).thenReturn(new Date(now));
        schedulerService.scheduleJob(JOB_ID_1, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1).withRunMode(RUN_LOCALLY));
        reset(clusteredJobDao, queue);

        when(clusteredJobDao.getNextRunTime(JOB_ID_1)).thenReturn(new Date(now + 4200L));
        schedulerService.refreshClusteredJob(JOB_ID_1);

        verify(clusteredJobDao).getNextRunTime(JOB_ID_1);
        verify(queue).add(new QueuedJob(JOB_ID_1, now + 4200L));
        verifyNoMoreInteractions(clusteredJobDao, queue);

        assertThat("local job should be zapped", schedulerService.getJobDetails(JOB_ID_1), nullValue());
    }

    @Test
    public void testRefreshClusteredJobs() throws SchedulerServiceException {
        when(calculator.firstRunTime(any(JobId.class), any(JobConfig.class))).thenReturn(new Date(now));
        schedulerService.scheduleJob(JOB_ID_1, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1)
                .withRunMode(RUN_LOCALLY)
                .withSchedule(runOnce(new Date(now + 1000L))));
        schedulerService.scheduleJob(JOB_ID_2, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1)
                .withRunMode(RUN_LOCALLY)
                .withSchedule(runOnce(new Date(now + 2000L))));
        when(queue.refreshClusteredJobs()).thenReturn(ImmutableMap.of(
                JOB_ID_2, new Date(now + 3000L),
                JOB_ID_3, new Date(now + 4000L)));

        schedulerService.refreshClusteredJobs();

        assertThat(schedulerService.getJobsByJobRunnerKey(JOB_RUNNER_KEY_1), contains(
                jobDetails(JOB_ID_1, JOB_RUNNER_KEY_1, RUN_LOCALLY,
                        runOnce(new Date(now + 1000L)), new Date(now), null)));
    }

    @Test
    public void testExecuteQueuedJobThatNoLongerExists() {
        schedulerService.executeQueuedJob(new QueuedJob(JOB_ID_1, now));

        final RunDetails runDetails = runDetailsDao.getLastRunForJob(JOB_ID_1);
        assertThat("Should not record anything", runDetails, nullValue());
    }

    @Test
    public void testExecuteLocalJobTooEarly() throws SchedulerServiceException {
        when(calculator.firstRunTime(any(JobId.class), any(JobConfig.class))).thenReturn(new Date(now));
        schedulerService.scheduleJob(JOB_ID_1, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1)
                .withRunMode(RUN_LOCALLY)
                .withSchedule(runOnce(new Date(now))));
        final QueuedJob job = new QueuedJob(JOB_ID_1, now);
        reset(queue);

        now -= 1000L;  // Time travel is awesome
        schedulerService.executeQueuedJob(job);

        // Since we are too early, the assumption is that this is a race condition where another thread
        // has changed the job's schedule at the same time as we started to launch it.  The end result is
        // that we put the job back in the queue for the correct time and go back to sleep without taking
        // any further action.
        verify(queue).add(job);
        assertThat(runDetailsDao.getLastRunForJob(JOB_ID_1), nullValue());
    }

    @Test
    public void testExecuteLocalJobOnTime() throws SchedulerServiceException {
        when(calculator.firstRunTime(any(JobId.class), any(JobConfig.class))).thenReturn(new Date(now));
        when(calculator.nextRunTime(forInterval(60000L, null), new Date(now))).thenReturn(new Date(now + 60000L));
        schedulerService.registerJobRunner(JOB_RUNNER_KEY_1, new TestJobRunner());
        schedulerService.scheduleJob(JOB_ID_1, JobConfig.forJobRunnerKey(JOB_RUNNER_KEY_1)
                .withRunMode(RUN_LOCALLY)
                .withSchedule(forInterval(60000L, null)));
        final QueuedJob job = new QueuedJob(JOB_ID_1, now);
        reset(queue);

        schedulerService.executeQueuedJob(job);

        // The job fires normally.  It should be rescheduled for its next run time and record a successful run result.
        verify(queue).add(new QueuedJob(JOB_ID_1, now + 60000L));

        final RunDetails runDetails = runDetailsDao.getLastRunForJob(JOB_ID_1);
        assertThat("runOutcome", runDetails.getRunOutcome(), equalTo(RunOutcome.SUCCESS));
    }

    @Test
    public void testExecuteClusteredJobTooEarly() throws SchedulerServiceException {
        schedulerService.registerJobRunner(JOB_RUNNER_KEY_1, new TestJobRunner());
        when(clusteredJobDao.find(JOB_ID_1)).thenReturn(ImmutableClusteredJob.builder()
                .jobId(JOB_ID_1)
                .jobRunnerKey(JOB_RUNNER_KEY_1)
                .schedule(Schedule.runOnce(new Date(now + 5000L)))
                .nextRunTime(new Date(now + 5000L))
                .version(2)
                .build());

        schedulerService.executeQueuedJob(new QueuedJob(JOB_ID_1, now));

        // Job just gets rescheduled
        verify(queue).add(new QueuedJob(JOB_ID_1, now + 5000L));
        assertThat(runDetailsDao.getLastRunForJob(JOB_ID_1), nullValue());
    }

    @Test
    public void testExecuteClusteredJobLostRaceToAnotherNode() throws SchedulerServiceException {
        schedulerService.registerJobRunner(JOB_RUNNER_KEY_1, new TestJobRunner());
        final long later = now + 60000L;

        final ClusteredJob job = ImmutableClusteredJob.builder()
                .jobId(JOB_ID_1)
                .jobRunnerKey(JOB_RUNNER_KEY_1)
                .schedule(Schedule.forInterval(60000L, null))
                .nextRunTime(new Date(now))
                .version(2)
                .build();
        when(clusteredJobDao.find(JOB_ID_1)).thenReturn(job);
        when(calculator.nextRunTime(Schedule.forInterval(60000L, null), new Date(now))).thenReturn(new Date(later));
        when(clusteredJobDao.getNextRunTime(JOB_ID_1)).thenReturn(new Date(later));

        schedulerService.executeQueuedJob(new QueuedJob(JOB_ID_1, now));

        verify(clusteredJobDao).updateNextRunTime(JOB_ID_1, new Date(later), 2L);
        verify(queue).add(new QueuedJob(JOB_ID_1, later));
        assertThat(runDetailsDao.getLastRunForJob(JOB_ID_1), nullValue());
    }

    @Test
    public void testExecuteClusteredJobRuntimeExceptionDuringClaimTriggersRecovery() throws SchedulerServiceException {
        schedulerService.registerJobRunner(JOB_RUNNER_KEY_1, new TestJobRunner());
        final long recoverAt = now + 60000L;
        final long later = now + 120000L;

        final ClusteredJob job = ImmutableClusteredJob.builder()
                .jobId(JOB_ID_1)
                .jobRunnerKey(JOB_RUNNER_KEY_1)
                .schedule(Schedule.forInterval(120000L, null))
                .nextRunTime(new Date(now))
                .version(2)
                .build();
        when(clusteredJobDao.find(JOB_ID_1)).thenReturn(job);
        when(calculator.nextRunTime(Schedule.forInterval(120000L, null), new Date(now))).thenReturn(new Date(later));
        when(clusteredJobDao.getNextRunTime(JOB_ID_1)).thenReturn(new Date(later));
        when(clusteredJobDao.updateNextRunTime(JOB_ID_1, new Date(later), 2L)).thenAnswer(invocation -> {
            throw new RuntimeException("Ouch!");
        });

        schedulerService.executeQueuedJob(new QueuedJob(JOB_ID_1, now));

        verify(clusteredJobDao).updateNextRunTime(JOB_ID_1, new Date(later), 2L);
        verify(queue).add(new QueuedJob(RECOVERY_JOB_ID, recoverAt));
        assertThat(schedulerService.getJobRunner(RECOVERY_JOB_RUNNER_KEY),
                instanceOf(CaesiumSchedulerService.RecoveryJob.class));
        assertThat(runDetailsDao.getLastRunForJob(JOB_ID_1), nullValue());
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void testRefreshJobWithNullIdRejectsWithoutTriggeringRecovery() throws SchedulerServiceException {
        reject(clusteredJobDao).getNextRunTime(any(JobId.class));
        reject(queue).add(any(QueuedJob.class));

        schedulerService.refreshClusteredJob(null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = IllegalArgumentException.class)
    public void testRefreshJobWithBlankIdRejectsWithoutTriggeringRecovery() throws SchedulerServiceException {
        reject(clusteredJobDao).getNextRunTime(any(JobId.class));
        reject(queue).add(any(QueuedJob.class));

        schedulerService.refreshClusteredJob(JobId.of(" "));
    }

    @Test
    public void testExecuteClusteredJobSuccess() throws SchedulerServiceException {
        clusteredJobDao = new MemoryClusteredJobDao();
        createSchedulerService();
        schedulerService.registerJobRunner(JOB_RUNNER_KEY_1, new TestJobRunner());
        final long later = now + 60000L;

        assertThat("Couldn't create clustered job?", clusteredJobDao.create(ImmutableClusteredJob.builder()
                .jobId(JOB_ID_1)
                .jobRunnerKey(JOB_RUNNER_KEY_1)
                .schedule(Schedule.forInterval(60000L, null))
                .nextRunTime(new Date(now))
                .build()), is(true));
        when(calculator.nextRunTime(Schedule.forInterval(60000L, null), new Date(now))).thenReturn(new Date(later));

        schedulerService.executeQueuedJob(new QueuedJob(JOB_ID_1, now));

        verify(queue).add(new QueuedJob(JOB_ID_1, later));
        assertThat(clusteredJobDao.getNextRunTime(JOB_ID_1), is(new Date(later)));
        final RunDetails runDetails = runDetailsDao.getLastRunForJob(JOB_ID_1);
        assertThat("runOutcome", runDetails.getRunOutcome(), equalTo(RunOutcome.SUCCESS));
    }

    private static Matcher<JobDetails> jobDetails(final JobId jobId, final JobRunnerKey jobRunnerKey,
                                                  final RunMode runMode, final Schedule schedule,
                                                  @Nullable final Date nextRunTime,
                                                  @Nullable final byte[] rawParameters) {
        return new JobDetailsMatcher(jobId, jobRunnerKey, runMode, schedule, nextRunTime, rawParameters);
    }

    private static class TestJobRunner implements JobRunner {
        @Nullable
        @Override
        public JobRunnerResponse runJob(JobRunnerRequest request) {
            return null;
        }
    }

    @SuppressWarnings({"AssignmentToDateFieldFromParameter", "AssignmentToCollectionOrArrayFieldFromParameter"})
    private static class JobDetailsMatcher extends TypeSafeMatcher<JobDetails> {
        private final JobId jobId;
        private final JobRunnerKey jobRunnerKey;
        private final RunMode runMode;
        private final Schedule schedule;

        @Nullable
        private final Date nextRunTime;

        @Nullable
        private final byte[] rawParameters;

        JobDetailsMatcher(JobId jobId, JobRunnerKey jobRunnerKey, RunMode runMode, Schedule schedule,
                                 @Nullable Date nextRunTime, @Nullable byte[] rawParameters) {
            this.jobId = jobId;
            this.jobRunnerKey = jobRunnerKey;
            this.runMode = runMode;
            this.schedule = schedule;
            this.nextRunTime = nextRunTime;
            this.rawParameters = rawParameters;
        }

        @Override
        protected boolean matchesSafely(JobDetails job) {
            try {
                final byte[] jobParameters = new ParameterMapSerializer().serializeParameters(job.getParameters());
                return job.getJobId().equals(jobId)
                        && job.getJobRunnerKey().equals(jobRunnerKey)
                        && job.getRunMode() == runMode
                        && job.getSchedule().equals(schedule)
                        && Objects.equal(job.getNextRunTime(), nextRunTime)
                        && Arrays.equals(jobParameters, rawParameters);
            } catch (SchedulerServiceException sse) {
                return false;
            }
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("a JobDetails using [jobId=").appendText(jobId.toString())
                    .appendText(",jobRunnerKey=").appendText(jobRunnerKey.toString())
                    .appendText(",runMode=").appendText(runMode.toString())
                    .appendText(",schedule=").appendText(schedule.toString())
                    .appendText(",nextRunTime=").appendText(String.valueOf(nextRunTime))
                    .appendText(",parameters=").appendText(parameterDescription(rawParameters));
        }

        private String parameterDescription(@Nullable final byte[] rawParameters) {
            if (rawParameters != null) {
                return "(" + rawParameters.length + " bytes)";
            }
            return "null";
        }
    }
}
