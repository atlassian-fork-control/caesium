package com.atlassian.scheduler.caesium.cron.parser;

import com.atlassian.scheduler.cron.CronSyntaxException;
import com.atlassian.scheduler.cron.ErrorCode;

import javax.annotation.Nullable;

import static com.atlassian.scheduler.cron.ErrorCode.ILLEGAL_CHARACTER;
import static com.atlassian.scheduler.cron.ErrorCode.UNEXPECTED_END_OF_EXPRESSION;
import static com.atlassian.scheduler.cron.ErrorCode.UNEXPECTED_TOKEN_FLAG_L;
import static com.atlassian.scheduler.cron.ErrorCode.UNEXPECTED_TOKEN_FLAG_W;
import static com.atlassian.scheduler.cron.ErrorCode.UNEXPECTED_TOKEN_HASH;
import static java.util.Objects.requireNonNull;

/**
 * Lexer/tokenizer for cron expressions.
 * <p>
 * See {@link TokenType} for the various token types and their meanings.
 * </p>
 *
 * @since v0.0.1
 */
class CronLexer {
    private final String cronExpression;

    private Token peeked;
    private int pos;

    CronLexer(final String cronExpression) {
        this.cronExpression = requireNonNull(cronExpression, "cronExpression");
    }

    boolean hasMoreTokens() {
        return peekToken().getType() != TokenType.NOTHING;
    }

    /**
     * Returns the next token without claiming it.
     *
     * @return the next token without claiming it.
     */
    Token peekToken() {
        if (peeked == null) {
            peeked = nextTokenInternal();
        }
        return peeked;
    }

    /**
     * Returns the next token and claims it.
     *
     * @return the next token and claims it.
     */
    Token nextToken() {
        if (peeked != null) {
            final Token token = peeked;
            peeked = null;
            return token;
        }
        return nextTokenInternal();
    }

    private Token nextTokenInternal() {
        final int len = cronExpression.length();
        if (pos >= len) {
            return new Token(TokenType.NOTHING, len, len);
        }

        return nextToken(cronExpression.charAt(pos));
    }

    /**
     * Moves the lexer to the specified token.
     * <p>
     * This can be used to implement a look-ahead by holding onto a peeked token, attempting one possible
     * parsing route, then moving back to the peeked token to reset for the alternate pathway.  Any tokens
     * that follow the one specified will have to be re-parsed; the lexer will not remember them.
     * </p>
     *
     * @param token the token at which to reposition the parser
     */
    void moveTo(Token token) {
        this.peeked = token;
        this.pos = token.getEnd();
    }

    @SuppressWarnings({"MethodWithMultipleReturnPoints", "OverlyComplexMethod"})
    private Token nextToken(final char c) {
        switch (c) {
            case ',':
                return token(TokenType.COMMA);
            case '-':
                return token(TokenType.HYPHEN);
            case '*':
                return token(TokenType.ASTERISK);
            case '/':
                return token(TokenType.SLASH);
            case '?':
                return token(TokenType.QUESTION_MARK);
            case '#':
                return token(TokenType.HASH);
            case ' ':
            case '\t':
                return whitespace();
        }

        if (isDigit(c)) {
            return number();
        }

        if (isUpper(c)) {
            return name();
        }

        return token(TokenType.INVALID);
    }

    private Token token(TokenType type) {
        return token(type, pos + 1);
    }

    private Token token(TokenType type, int end) {
        final Token token = new Token(type, pos, end);
        pos = end;
        return token;
    }

    private Token number() {
        final int len = cronExpression.length();
        int i = pos + 1;
        while (i < len && isDigit(cronExpression.charAt(i))) {
            ++i;
        }
        return token(TokenType.NUMBER, i);
    }

    private Token whitespace() {
        final int len = cronExpression.length();
        int i = pos + 1;
        while (i < len && isSpace(cronExpression.charAt(i))) {
            ++i;
        }
        return token(TokenType.WHITESPACE, i);
    }

    private Token name() {
        final int len = cronExpression.length();
        int i = pos + 1;
        while (i < len && isUpper(cronExpression.charAt(i))) {
            ++i;
        }
        return name(i);
    }

    private Token name(final int end) {
        switch (end - pos) {
            case 1:
                return upperLen1();
            case 2:
                return upperLen2();
        }
        return token(TokenType.NAME, end);
    }

    // The only valid length 1 uppercase sequences are 'L' and 'W'.
    // Anything else we return as a name and let the lookup fail.
    private Token upperLen1() {
        switch (cronExpression.charAt(pos)) {
            case 'L':
                return token(TokenType.FLAG_L);
            case 'W':
                return token(TokenType.FLAG_W);
        }
        return token(TokenType.NAME);
    }

    // The only valid length 2 uppercase sequence is 'LW'.
    // Anything else we return as a name and let the lookup fail.
    private Token upperLen2() {
        if (cronExpression.charAt(pos) == 'L' && cronExpression.charAt(pos + 1) == 'W') {
            return token(TokenType.FLAG_L);
        }
        return token(TokenType.NAME, pos + 2);
    }

    private static boolean isSpace(final char c) {
        return c == ' ' || c == '\t';
    }

    private static boolean isDigit(final char c) {
        return c >= '0' && c <= '9';
    }

    private static boolean isUpper(final char c) {
        return c >= 'A' && c <= 'Z';
    }


    /**
     * Represents a lexical token from a cron expression.
     *
     * @since v0.0.1
     */
    class Token {
        private final TokenType type;
        private final int start;
        private final int end;

        Token(TokenType type, int start, int end) {
            this.type = requireNonNull(type, "type");
            this.start = start;
            this.end = end;
        }

        String getCronExpression() {
            return cronExpression;
        }

        TokenType getType() {
            return type;
        }

        int getStart() {
            return start;
        }

        int getEnd() {
            return end;
        }

        String getText() {
            if (start == end) {
                return "";
            }
            return cronExpression.substring(start, end);
        }

        char getChar() {
            if (start == end) {
                throw new IllegalStateException("Called getChar() on zero-length token: " + this);
            }
            return cronExpression.charAt(start);
        }

        @Override
        public boolean equals(@Nullable Object o) {
            return this == o || (o instanceof Token && equals((Token) o));
        }

        private boolean equals(Token other) {
            return type == other.type && start == other.start && end == other.end;
        }

        @Override
        public int hashCode() {
            int result = type.hashCode();
            result = 31 * result + start;
            result = 31 * result + end;
            return result;
        }

        @Override
        public String toString() {
            return "Token[" + type + '[' + getText() + "],start=" + start + ']';
        }

        CronSyntaxException unexpected() {
            switch (type) {
                case HASH:
                    return syntaxError(UNEXPECTED_TOKEN_HASH).build();
                case FLAG_L:
                    return syntaxError(UNEXPECTED_TOKEN_FLAG_L).build();
                case FLAG_W:
                    return syntaxError(UNEXPECTED_TOKEN_FLAG_W).build();
                case NOTHING:
                    return syntaxError(UNEXPECTED_END_OF_EXPRESSION).build();
            }

            return syntaxError(ILLEGAL_CHARACTER)
                    .value(cronExpression.charAt(start))
                    .build();
        }

        private CronSyntaxException.Builder syntaxError(final ErrorCode errorCode) {
            return CronSyntaxException.builder()
                    .cronExpression(cronExpression)
                    .errorCode(errorCode)
                    .errorOffset(start);
        }
    }
}
