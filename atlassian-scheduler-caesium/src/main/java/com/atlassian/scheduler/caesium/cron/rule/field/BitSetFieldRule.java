package com.atlassian.scheduler.caesium.cron.rule.field;

import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate;
import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate.Field;

import java.util.BitSet;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;

/**
 * A cron field implementation that is based on a bit set of accepted values.
 *
 * @since v0.0.1
 */
public class BitSetFieldRule extends AbstractFieldRule {
    private static final long serialVersionUID = 4225435919726324942L;

    private final BitSet values;

    private BitSetFieldRule(Field field, BitSet values) {
        super(field);
        this.values = (BitSet) values.clone();
    }

    /**
     * Selects the most efficient implementation for the bit set provided.
     * <p>
     * If {@code values} contains bits that are in a single contiguous group, then the faster
     * {@link RangeFieldRule} implementation is substituted.
     * </p>
     *
     * @param field  which field to access when working with {@link DateTimeTemplate}
     * @param values a {@code BitSet} containing that accepted values for this field; must not be empty
     * @return a field rule that implements the specified behaviour
     */
    public static FieldRule of(Field field, BitSet values) {
        requireNonNull(values, "values");

        final int firstSetBit = values.nextSetBit(0);
        checkArgument(firstSetBit != -1, "values cannot be empty");
        checkArgument(firstSetBit >= field.getMinimumValue(),
                "values cannot contain bits less than the field minimum");
        checkArgument(values.nextSetBit(field.getMaximumValue() + 1) == -1,
                "values cannot contain bits more than the field maximum");

        final int nextClearBit = values.nextClearBit(firstSetBit + 1);
        final int separatedBit = values.nextSetBit(nextClearBit + 1);

        if (separatedBit == -1) {
            // The bits are limited to a single range, so we can use the more efficient RangeFieldRule, instead.
            return RangeFieldRule.of(field, firstSetBit, nextClearBit - 1);
        }

        return new BitSetFieldRule(field, values);
    }

    @Override
    public boolean matches(DateTimeTemplate dateTime) {
        return values.get(get(dateTime));
    }

    @Override
    public boolean next(DateTimeTemplate dateTime) {
        int value = values.nextSetBit(get(dateTime) + 1);
        if (value == -1) {
            return false;
        }
        set(dateTime, value);
        return true;
    }

    @Override
    public boolean first(DateTimeTemplate dateTime) {
        set(dateTime, values.nextSetBit(field.getMinimumValue()));
        return true;
    }

    @Override
    protected void appendTo(final StringBuilder sb) {
        int bit = values.nextSetBit(field.getMinimumValue());
        bit = appendRangeTo(sb, bit);
        bit = values.nextSetBit(bit);

        while (bit != -1) {
            sb.append(',');
            bit = appendRangeTo(sb, bit);
            bit = values.nextSetBit(bit);
        }
    }

    private int appendRangeTo(final StringBuilder sb, final int startBit) {
        sb.append(startBit);

        final int nextBit = values.nextClearBit(startBit + 1);
        final int lastBit = nextBit - 1;
        if (lastBit > startBit) {
            sb.append('-').append(lastBit);
        }

        return nextBit;
    }
}

