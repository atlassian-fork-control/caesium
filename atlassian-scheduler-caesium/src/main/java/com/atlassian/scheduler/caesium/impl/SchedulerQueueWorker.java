package com.atlassian.scheduler.caesium.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Consumer;

import static java.util.Objects.requireNonNull;

/**
 * Works the scheduler queue by identifying jobs that are pending execution and telling
 * the scheduler when it's time to run them.
 *
 * @since v0.0.1
 */
class SchedulerQueueWorker implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(SchedulerQueueWorker.class);

    private final SchedulerQueue queue;
    private final Consumer<QueuedJob> executeJob;

    /**
     * @param queue      the scheduler queue from which to take jobs
     * @param executeJob hands off jobs by passing them to this
     */
    SchedulerQueueWorker(SchedulerQueue queue, Consumer<QueuedJob> executeJob) {
        this.queue = requireNonNull(queue, "queue");
        this.executeJob = requireNonNull(executeJob, "executeJob");
    }

    @Override
    public void run() {
        for (; ; ) {
            try {
                if (!executeNextJob()) {
                    break;
                }
            } catch (InterruptedException ie) {
                LOG.debug("Scheduler queue worker was interrupted; ignoring...", ie);
            }
        }

        LOG.debug("Shutting down.");
    }

    /**
     * Take the next job from the queue and pass it back to the sink for execution.
     *
     * @return {@code true} if a job has been successfully taken and executed; {@code false} if this
     * failed because the working queue has closed (the scheduler is shutting down).
     * @throws InterruptedException if the worker thread is interrupted while waiting for work
     */
    private boolean executeNextJob() throws InterruptedException {
        final QueuedJob job = queue.take();
        if (job == null) {
            LOG.debug("The scheduler queue has closed.");
            return false;
        }

        executeJob(job);
        return true;
    }

    private void executeJob(QueuedJob job) {
        try {
            executeJob.accept(job);
        } catch (Throwable e) {
            // This isn't really supposed to happen, and if it is something like
            // an OutOfMemoryError then even logging this message might end up
            // just throwing another exception, but this is as good as it gets.
            LOG.error("Unhandled exception thrown by job {}", job, e);
        }
    }
}
