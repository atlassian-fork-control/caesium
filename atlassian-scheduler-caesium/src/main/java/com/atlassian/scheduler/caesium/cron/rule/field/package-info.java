/**
 * Rules that work with the value of a single field of {@link com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate}.
 */
@ParametersAreNonnullByDefault package com.atlassian.scheduler.caesium.cron.rule.field;

import javax.annotation.ParametersAreNonnullByDefault;