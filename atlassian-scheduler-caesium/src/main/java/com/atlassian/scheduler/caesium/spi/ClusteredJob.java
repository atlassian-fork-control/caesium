package com.atlassian.scheduler.caesium.spi;

import com.atlassian.scheduler.caesium.impl.ImmutableClusteredJob;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;

/**
 * The concrete objects that must be persisted by {@link ClusteredJobDao}.
 * <p>
 * Although the description given here assumes a single table with one row per scheduler entry, the
 * application is free to normalize the schema as desired, so long as it is still able to guarantee
 * the atomicity of the operations defined by the {@link ClusteredJobDao}.  There is an implementation
 * of this interface available as {@link ImmutableClusteredJob}.
 * </p>
 * <table summary="Datamodel Information">
 * <thead>
 * <tr><th>Field</th><th>Type</th><th>Size/Resolution</th><th>Comment</th></tr>
 * </thead>
 * <tbody>
 * <tr><td>JobId</td><td>text</td><td>255</td><td>Use {@link JobId#of(String)} and {@link JobId#toString()}
 * to convert to and from storage</td></tr>
 * <tr><td>JobRunnerKey</td><td>text</td><td>255</td><td>Use {@link JobRunnerKey#of(String)} and
 * {@link JobRunnerKey#toString()} to convert to and from storage</td></tr>
 * <tr><td>Interval</td><td>numeric</td><td>Millis</td><td>{@code NULL} for cron expression schedules.</td></tr>
 * <tr><td>Cron Expression</td><td>text</td><td>120</td><td>{@code NULL} for interval schedules.</td></tr>
 * <tr><td>Time Zone</td><td>text</td><td>80</td><td>{@code NULL} for interval schedules.</td></tr>
 * <tr><td>Next Run Time</td><td>date/time</td><td>Millis</td><td>{@code NULL} when the job is not
 * expected to run anymore due to its schedule rules</td></tr>
 * <tr><td>Version</td><td>numeric</td><td>long</td><td>Used for optimistic locking strategy</td></tr>
 * <tr><td>Parameters</td><td>blob</td><td>unspecified</td><td>{@code NULL} when the job has no parameters</td></tr>
 * </tbody>
 * </table>
 *
 * @since v0.0.1
 */
public interface ClusteredJob {
    @Nonnull
    JobId getJobId();

    @Nonnull
    JobRunnerKey getJobRunnerKey();

    @Nonnull
    Schedule getSchedule();

    @Nullable
    Date getNextRunTime();

    long getVersion();

    @Nullable
    byte[] getRawParameters();
}
