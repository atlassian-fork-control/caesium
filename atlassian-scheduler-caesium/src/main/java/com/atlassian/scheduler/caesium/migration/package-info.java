/**
 * Tools to make it easier to migrate from Quartz to Caesium.
 */
@ParametersAreNonnullByDefault package com.atlassian.scheduler.caesium.migration;

import javax.annotation.ParametersAreNonnullByDefault;