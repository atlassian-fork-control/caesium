package com.atlassian.scheduler.caesium.cron.rule.field;

import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate;
import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate.Field;

import static java.util.Objects.requireNonNull;

/**
 * The base implementation for manipulating date information according to a cron expression's rules.
 *
 * @since v0.0.1
 */
abstract class AbstractFieldRule implements FieldRule {
    private static final long serialVersionUID = 8456955222871836404L;

    protected final Field field;

    AbstractFieldRule(Field field) {
        this.field = requireNonNull(field, "dateTimeFieldType");
    }

    @Override
    public int get(DateTimeTemplate dateTime) {
        return field.get(dateTime);
    }

    @Override
    public void set(DateTimeTemplate dateTime, int value) {
        field.set(dateTime, value);
    }


    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder(64).append(field).append(": ");
        appendTo(sb);
        return sb.toString();
    }

    /**
     * Reports the information about how this field is configured.
     * For troubleshooting reasons, it may be necessary to display the rules that were actually
     * parsed for a cron expression.  Each field rule is responsible for implementing this
     * method in a way that unambiguously identifies what the rule means.
     *
     * @param sb a buffer to append with the field's information
     */
    protected abstract void appendTo(StringBuilder sb);
}
