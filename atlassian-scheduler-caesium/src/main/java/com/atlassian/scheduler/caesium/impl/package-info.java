/**
 * The Caesium implementation of atlassian-scheduler.
 * <p>
 * The central class is {@link com.atlassian.scheduler.caesium.impl.CaesiumSchedulerService}.
 * </p>
 */
@ParametersAreNonnullByDefault package com.atlassian.scheduler.caesium.impl;

import javax.annotation.ParametersAreNonnullByDefault;
