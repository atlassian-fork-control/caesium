package com.atlassian.scheduler.caesium.cron.parser;

import com.atlassian.scheduler.caesium.cron.parser.CronLexer.Token;
import com.atlassian.scheduler.cron.CronSyntaxException;
import com.atlassian.scheduler.cron.ErrorCode;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NAME_DAY_OF_WEEK;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NAME_MONTH;

/**
 * Attempts to translate English names for months or days of the week.
 *
 * @since v0.0.1
 */
class NameResolver {
    private static final ImmutableMap<String, Integer> MONTH_NAMES = ImmutableMap.<String, Integer>builder()
            .put("JAN", 1)
            .put("FEB", 2)
            .put("MAR", 3)
            .put("APR", 4)
            .put("MAY", 5)
            .put("JUN", 6)
            .put("JUL", 7)
            .put("AUG", 8)
            .put("SEP", 9)
            .put("OCT", 10)
            .put("NOV", 11)
            .put("DEC", 12)
            .build();

    private static final ImmutableMap<String, Integer> DAY_OF_WEEK_NAMES = ImmutableMap.<String, Integer>builder()
            .put("SUN", 1)
            .put("MON", 2)
            .put("TUE", 3)
            .put("WED", 4)
            .put("THU", 5)  // *Not* THR!
            .put("FRI", 6)
            .put("SAT", 7)
            .build();

    static final NameResolver MONTH = new NameResolver(MONTH_NAMES, INVALID_NAME_MONTH);
    static final NameResolver DAY_OF_WEEK = new NameResolver(DAY_OF_WEEK_NAMES, INVALID_NAME_DAY_OF_WEEK);

    private final Map<String, Integer> names;
    private final ErrorCode errorCode;

    private NameResolver(final ImmutableMap<String, Integer> names, final ErrorCode errorCode) {
        this.names = names;
        this.errorCode = errorCode;
    }

    final int resolveName(Token token) throws CronSyntaxException {
        if (token.getType() != TokenType.NAME) {
            throw new IllegalArgumentException("Called name resolver with something that wasn't a NAME: " + token);
        }

        final String name = token.getText();
        final Integer value = names.get(name);
        if (value == null) {
            throw CronSyntaxException.builder()
                    .cronExpression(token.getCronExpression())
                    .errorCode(errorCode)
                    .errorOffset(token.getStart())
                    .value(name)
                    .build();
        }
        return value;
    }
}

