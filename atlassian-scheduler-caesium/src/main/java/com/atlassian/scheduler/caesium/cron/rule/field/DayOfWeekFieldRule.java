package com.atlassian.scheduler.caesium.cron.rule.field;

import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate;
import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate.Field;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;

import java.io.Serializable;
import java.util.BitSet;

import static com.atlassian.scheduler.caesium.cron.rule.field.DayOfWeekConstantConverter.cronToIso;
import static com.atlassian.scheduler.caesium.cron.rule.field.DayOfWeekConstantConverter.isoToName;
import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;

/**
 * Rules based on target days of the week without any special flags.
 * <p>
 * This field works a bit differently from any of the others and needs its own implementation rather than
 * using {@link BitSetFieldRule}.  The day-of-week value is not stored as one of the items in
 * {@link DateTimeTemplate} because it is not independent &mdash; it is a function of the the year, month,
 * and day-of-month values.  Moving to another day-of-week value is implemented in terms of moving to the
 * next day-of-month value for which that function yields the desired result while holding the year and month
 * values fixed.  To add to the fun, we also need to convert between the cron and ISO numbering conventions
 * for the day-of-week value.
 * </p>
 *
 * @since v0.0.1
 */
public class DayOfWeekFieldRule extends AbstractFieldRule {
    private static final long serialVersionUID = -5922219253093923249L;
    private static final DayOfWeekFieldRule SATURDAY = new DayOfWeekFieldRule();

    private final BitSet isoDaysOfWeek;

    /**
     * Returns a singleton that represents matching on Saturday only.
     * This is admittedly pretty useless, but it's what happens if somebody uses the special value {@code L}
     * for the day-of-week field.  We would otherwise have to build up a new {@code isoDaysOfWeek} bitset
     * with just a {@code 6} in it anyway, and since the rules are stateless we can just reuse this same
     * one forever.
     *
     * @return a singleton that represents matching on Saturday only.
     */
    public static DayOfWeekFieldRule saturday() {
        return SATURDAY;
    }

    // Special constructor for Saturday
    private DayOfWeekFieldRule() {
        super(Field.DAY);

        final BitSet saturday = new BitSet(8);
        saturday.set(DateTimeConstants.SATURDAY);  // 6, the ISO number for it
        this.isoDaysOfWeek = saturday;
    }

    private DayOfWeekFieldRule(final BitSet values) {
        super(Field.DAY);

        requireNonNull(values, "values");
        checkArgument(!values.isEmpty(), "values cannot be empty");
        checkArgument(!values.get(0), "values cannot contain 0");
        checkArgument(values.nextSetBit(8) == -1, "values cannot contain anything > 7");

        this.isoDaysOfWeek = cronToIso(values);
    }

    public static FieldRule of(final BitSet values) {
        return new DayOfWeekFieldRule(values);
    }

    @Override
    public boolean matches(DateTimeTemplate dateTime) {
        final LocalDate firstOfMonth = dateTime.toFirstOfMonth();
        final int lastDayOfMonth = firstOfMonth.dayOfMonth().getMaximumValue();
        if (dateTime.getDay() > lastDayOfMonth) {
            return false;
        }

        final int isoDayOfWeek = getIsoDayOfWeek(firstOfMonth, dateTime.getDay());
        return isoDaysOfWeek.get(isoDayOfWeek);
    }

    @Override
    public boolean first(DateTimeTemplate dateTime) {
        final int isoDayOfWeek = dateTime.toFirstOfMonth().getDayOfWeek();
        if (isoDaysOfWeek.get(isoDayOfWeek)) {
            // For this month, the first matching day-of-week happens to also be the first of the month
            dateTime.setDay(1);
        } else {
            // Otherwise, we must move forward by however much it takes to find the first matching value
            dateTime.setDay(1 + interval(isoDayOfWeek));
        }
        return true;
    }

    @Override
    public boolean next(DateTimeTemplate dateTime) {
        final LocalDate firstOfMonth = dateTime.toFirstOfMonth();
        final int isoDayOfWeek = getIsoDayOfWeek(firstOfMonth, dateTime.getDay());
        final int day = dateTime.getDay() + interval(isoDayOfWeek);
        final int lastDayOfMonth = firstOfMonth.dayOfMonth().getMaximumValue();

        if (day > lastDayOfMonth) {
            // The next matching day-of-week value doesn't happen until next month, so we're done for now.
            return false;
        }

        dateTime.setDay(day);
        return true;
    }

    /**
     * Returns the number of days to move forward within the month in order to find the next matching day of the week.
     *
     * @param isoDayOfWeek the current ISO day of the week (1=Monday .. 7=Sunday)
     * @return the number of days to move forward in order to find the next matching day of the week
     */
    private int interval(int isoDayOfWeek) {
        int next = isoDaysOfWeek.nextSetBit(isoDayOfWeek + 1);
        if (next != -1) {
            // There are more days left in this same ISO week, such as when the current day is a Wednesday
            // and Friday would work.  This would return 2 for that case.
            return next - isoDayOfWeek;
        }

        // We have to wrap around to next week to find one, so we also need to adjust the (negative) interval by
        // adding 7 to keep it moving forward in time.
        next = isoDaysOfWeek.nextSetBit(1);
        return next - isoDayOfWeek + 7;
    }

    @Override
    protected void appendTo(StringBuilder sb) {
        int bit = isoDaysOfWeek.nextSetBit(1);
        if (bit == 1 && isoDaysOfWeek.nextClearBit(2) == 8) {
            // If all bits are set, then this is an '*' in the day-of-week field.  Report it
            // slightly differently so that it is clear that day-of-week is being used for this.
            sb.append("*(dow)");
            return;
        }

        // Reports each matching day-of-week as an individual value without trying to optimize it
        // into ranges.
        while (bit != -1) {
            sb.append(isoToName(bit)).append(',');
            bit = isoDaysOfWeek.nextSetBit(bit + 1);
        }
        sb.setLength(sb.length() - 1);
    }

    protected Object writeReplace() {
        //noinspection ObjectEquality
        return (this == SATURDAY) ? SaturdaySentinel.INSTANCE : this;
    }

    // Bypass Joda-time for this calculation, as it would check that the day-of-month is valid and
    // throw an exception at us if it isn't.  We don't want that, so we would have to perform our
    // own bounds checking anyway, and if we have to do that then letting it do the calculation is
    // not really buying us anything.
    private static int getIsoDayOfWeek(final LocalDate firstOfMonth, final int dayOfMonth) {
        final int dayOfWeek = (firstOfMonth.getDayOfWeek() + dayOfMonth - 1) % 7;
        return (dayOfWeek != 0) ? dayOfWeek : 7;
    }

    /**
     * A sentinel that is substituted for the {@link #SATURDAY} singleton when this rule is serialized.
     */
    static final class SaturdaySentinel implements Serializable {
        private static final long serialVersionUID = 1925593671579829200L;
        static final SaturdaySentinel INSTANCE = new SaturdaySentinel();

        protected Object readResolve() {
            return saturday();
        }
    }
}
