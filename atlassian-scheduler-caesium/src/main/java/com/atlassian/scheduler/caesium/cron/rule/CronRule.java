package com.atlassian.scheduler.caesium.cron.rule;

import java.io.Serializable;

/**
 * Something that can evaluate a rule (or set of rules) for a given {@link DateTimeTemplate} to confirm
 * whether or not it matches and advance to the next match that would satisfy it.
 * <p>
 * The rules themselves are stateless.
 * </p>
 *
 * @since v0.0.1
 */
public interface CronRule extends Serializable {
    /**
     * Returns {@code true} if {@code dateTime} matches the rule; {@code false} if it does not.
     *
     * @param dateTime the moment to consider as a match for the rule
     * @return {@code true} if {@code dateTime} matches the rule; {@code false} if it does not.
     */
    boolean matches(DateTimeTemplate dateTime);

    /**
     * Resets {@code dateTime} to the minimum matching value for this rule, if possible.
     *
     * @param dateTime the time to modify by resetting the value that this rule evaluates to its minimum matching value
     * @return {@code true} if {@code dateTime} has successfully been updated to when the rule would first match;
     * {@code false} if there are no matching values for this rule (at least without some other rule changing
     * {@code dateTime} in some other way first).
     */
    boolean first(DateTimeTemplate dateTime);

    /**
     * Advance {@code dateTime} to when the rule would next match, if possible.
     *
     * @param dateTime the time to modify by changing the value that this rule evaluates to its next matching value
     * @return {@code true} if {@code dateTime} has successfully been updated to when the rule would next match;
     * {@code false} if all legal values have been exhausted for this rule (at least without some other
     * rule changing {@code dateTime} in some other way first).
     */
    boolean next(DateTimeTemplate dateTime);
}
